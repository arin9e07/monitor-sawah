//
//  ViewController.swift
//  Monitor Sawah
//
//  Created by arinal haq on 10/12/20.
//

import UIKit
import FirebaseDatabase
import Firebase
import FirebaseAuth

class ViewController: UIViewController {
    var ref: DatabaseReference!

    @IBOutlet weak var kelembaban: UILabel!
    @IBOutlet weak var ketinggian: UILabel!
    @IBOutlet weak var ketinggianAir: UIImageView!
    @IBOutlet weak var kelembabanT: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
        
        
    }
    @objc func timerFired() {
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        ref.child("kelembaban").observeSingleEvent(of: .value, with: { [self] (snapshot) in
          // Get user value
          let value = snapshot.value as? NSNumber
            kelembaban.text = "\(value!)"
            let nilai = Double(value!)
            if nilai <= 50{
                kelembabanT.text = "Kering"
            }else if nilai > 50{
                kelembabanT.text = "Basah"
            }
//          let username = value?["username"] as? String ?? ""
//          let user = User(username: username)

          // ...
          }) { (error) in
            print(error.localizedDescription)
        }
        ref.child("ketinggian").observeSingleEvent(of: .value, with: { [self] (snapshot) in
          // Get user value
            let maxvalue = 20.0
          let value = snapshot.value as? NSNumber
            let nilai = Double(value!)
            let waterheight = maxvalue - nilai
            ketinggian.text = "\(Int(waterheight))"
            
            if waterheight <= 0{
                ketinggianAir.image = UIImage(named: "KetinggianAir-1")
            }
            else if waterheight <= 3{
                ketinggianAir.image = UIImage(named: "KetinggianAir-2")
            }
            else if waterheight <= 6{
                ketinggianAir.image = UIImage(named: "KetinggianAir-3")
            }
            else if waterheight <= 9{
                ketinggianAir.image = UIImage(named: "KetinggianAir-4")
            }
            else if waterheight <= 12{
                ketinggianAir.image = UIImage(named: "KetinggianAir-5")
            }
            else{
                ketinggianAir.image = UIImage(named: "KetinggianAir-6")
            }
            
//          let username = value?["username"] as? String ?? ""
//          let user = User(username: username)

          // ...
          }) { (error) in
            print(error.localizedDescription)
        }
          
    }
    
    


}

